
The data in this repository has been scraped from the OFCOM site and is provided here in a more accessible form only for ease of use/reference. The data is public domain and is used entirely at the risk of the downloader. We will endevour to keep the data in sync with the pages at OFCOM but no assurances are provided. This data may be stale.

Source pages
  * http://static.ofcom.org.uk/static/radiolicensing/html/radio-stations/community/community-main.htm
